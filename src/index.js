//Dans la variable data, je met
//Le local storage sous forme d'objet s'il existe
//un objet composé de deux tableaux vides dans le cas contraire

const data = (localStorage.getItem('data'))?JSON.parse(localStorage.getItem('data')):{
    todo: [],
    done:[]
};
if(localStorage.getItem('data')){
    console.log("la data existe dans le ls");
}
else {
    console.log('il faut initier la data');
}
function addToDoToLS(text){

};
addToDoToLS("hello");
let todoList = document.getElementById("todo");
let doneList = document.getElementById("done");
let deletedList = document.getElementById("deleted");
const addBtn = document.getElementById('addBtn');

// if(data){
//     if(data.todo){
//         data.todo.forEach((item)=>{
//             addToDo(item);
//             console.log("here");
//         });
//     };
// }
//function to remove a searched element from a specified array
function remove(searched, array){
    let flag = true;
    let count = 0;
    for (let element of array){
        if(flag && (element===searched)){
            array.splice(count, 1);
            flag=false;
        }
        count +=1;
    };
};
//Adds an html todo when given text content as an argument
function addToDo(text){
    let inlineDiv= document.createElement('div');
    let li = document.createElement('li');
    let btn = document.createElement('input');
    btn.type="radio";
    btn.className +="checkbox";
    li.innerText= text;
    inlineDiv.append(li);
    inlineDiv.append(btn);
    todoList.append(inlineDiv);
    btn.addEventListener('click', (e)=>{
        moveToDone(e);
    });
    addToDoData(text);
    //add to local Storage todo
};
//add a done element using text of cliked todo + css
function moveToDone(event){
    let itemDiv = event.target.parentNode;
    let itemText = itemDiv.innerText;
    itemDiv.className = "displayNone";
    let inlineDiv= document.createElement('div');
    let li = document.createElement('li');
    li.className+="crossed";
    let btn = document.createElement('button');
    btn.innerHTML="<img src='./public/assets/images/trashbin.png' alt='icon of bin' style='width:20px;height:20px;'/>";
    btn.className += "deleteBtn";
    li.innerText= itemText;
    inlineDiv.append(li);
    inlineDiv.append(btn);
    doneList.append(inlineDiv);
    btn.addEventListener('click', (e)=>{
        deleteThis(e);
    });
    //remove from local storage todo
    //add to local storage done
};
//puts a class on an element display:None it
function deleteThis(event){
    let itemDiv = event.target.parentNode.parentNode;
    let itemText = itemDiv.innerText;
    itemDiv.className = "displayNone";
    console.log(event);
    //delete from local storage
};
//User clicked on the add button, any text in the input must be added to the todolist
addBtn.addEventListener('click', ()=>{
    var inputText=document.getElementById('toAdd').value;
    //an empty string is always false
    if(inputText){
        console.log(inputText);
        addToDo(inputText);
    }
});

