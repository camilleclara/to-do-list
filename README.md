# TO DO LIST APPLICATION


# Webpack Seed evergreen for JavaScript and SASS

## This is an to do list project to practice pure JavaScript + Sass front-end coding out of the box

`npm install` at your first use, then

`npm start` to develop your application

`npm run build` to bundle your application. When it's done, deliver your `dist` folder: it contains anything needed.

